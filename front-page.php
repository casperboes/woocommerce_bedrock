<?php get_header();?>


<div id="home">

	<div class="container d-flex align-items-center justify-content-center h-100">

		<h1>Welkom in de winkel</h1>
		<h3>Wordt dit opgepakt door mijn Git?</h3>
		<h3>Eerste aanpassing in BitBucket</h3>

	</div>	
</div>

<div class="content">

	<div class="container">
	<?php if(have_posts()) : while(have_posts()) :the_post();?>
		<?php the_content();?>

	<?php endwhile; else: endif;?>
</div>
</div> 



<?php get_footer();?>